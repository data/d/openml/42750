# OpenML dataset: Census-Income-KDD

https://www.openml.org/d/42750

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Terran Lane and Ronny Kohavi. Data Mining and Visualization. Silicon Graphics  
**Source**: [original](https://archive.ics.uci.edu/ml/datasets/Census-Income+(KDD)) - 2000  
**Please cite**: Dua, D. and Graff, C. (2019). UCI Machine Learning Repository. Irvine, CA: University of California, School of Information and Computer Science.  

This version has feature names based on https://www2.1010data.com/documentationcenter/beta/Tutorials/MachineLearningExamples/CensusIncomeDataSet.html

Missing data is also properly encoded in this version.

The feature 'unknown' in the dataset does not appear in the list above. This possibly refers to the feature 'instance weight' in the original UCI description.

Feature                                                  Name

* Age of the worker                                                age

* Class of worker                                         class_worker

* Industry code                                           det_ind_code

* Occupation code                                         det_occ_code

* Level of education                                         education

* Wage per hour                                          wage_per_hour

* Enrolled in educational institution last week             hs_college

* Marital status                                          marital_stat

* Major industry code                                   major_ind_code

* Major occupation code                                 major_occ_code

* Race                                                            race

* Hispanic origin                                          hisp_origin

* Sex                                                              sex

* Member of a labor union                                 union_member

* Reason for unemployment                                 unemp_reason

* Full- or part-time employment status                full_or_part_emp

* Capital gains                                          capital_gains

* Capital losses                                        capital_losses

* Dividends from stocks                                stock_dividends

* Tax filer status                                      tax_filer_stat

* Region of previous residence                         region_prev_res

* State of previous residence                           state_prev_res

* Detailed household and family status                 det_hh_fam_stat

* Detailed household summary in household                  det_hh_summ

* Unknown                                             Unknown

* Migration code - change in MSA                           mig_chg_msa

* Migration code - change in region                        mig_chg_reg

* Migration code - move within region                     mig_move_reg

* Live in this house one year ago                             mig_same

* Migration - previous residence in sunbelt           mig_prev_sunbelt

* Number of persons that worked for employer                   num_emp

* Family members under 18                                 fam_under_18

* Country of birth father                               country_father

* Country of birth mother                               country_mother

* Country of birth                                        country_self

* Citizenship                                              citizenship

* Own business or self-employed?                           own_or_self

* Fill included questionnaire for Veterans Admin.      vet_question

* Veterans benefits                                       vet_benefits

* Weeks worked in the year                                weeks_worked

* Year of survey                                                  year

* Income less than or greater than 0,000                  income_50k

* Number of years of education                                edu_year

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42750) of an [OpenML dataset](https://www.openml.org/d/42750). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42750/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42750/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42750/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

